package cc.gemii.service;

import cc.gemii.pojo.CommonResult;

public interface MemberService {

	/**
	 * 
	 * @param displayName 群内昵称
	 * @param nickname 微信昵称
	 * @return
	 * TODO
	 */
	CommonResult selectMember(String displayName, String nickname);

	/**
	 * 
	 * @param memberID member ID
	 * @param tagID 标签ID
	 * @return
	 * TODO 管理员绑定标签
	 */
	CommonResult bindTag(String memberID, Integer tagID);

}
