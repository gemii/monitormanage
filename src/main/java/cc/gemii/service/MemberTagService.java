package cc.gemii.service;

import cc.gemii.po.Membertag;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;

public interface MemberTagService {

	/**
	 * 
	 * @param id 标签ID
	 * @param tag 标签名
	 * @param belong 1 gemii 2 wyeth
	 * @param page 分页
	 * @return
	 * TODO 获取标签
	 */
	CommonResult getTagList(Integer id, String search, Integer belong, Pagination page);

	/**
	 * 
	 * @param memberTag 
	 * @return
	 * TODO 新增标签
	 */
	CommonResult addTag(Membertag memberTag);

	/**
	 * 
	 * @param id 标签ID
	 * @return
	 * TODO 删除标签
	 */
	CommonResult deleteTag(Integer id);

}
