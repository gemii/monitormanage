package cc.gemii.service;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import cc.gemii.po.Roomkeyword;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;

public interface KeywordService {

	/**
	 * 
	 * @param roomKeyword 全局关键字包装类
	 * @return
	 * TODO
	 */
	CommonResult insertGlobalKeyword(Roomkeyword roomKeyword);

	/**
	 * 
	 * @param keyword 关键字名
	 * @param id 关键字ID
	 * @param belong 1 gemii 2 weyth
	 * @param page 分页
	 * @return
	 * TODO 获取全局关键字列表
	 */
	CommonResult getGlobalKeyword(Integer id, String keyword, Integer belong, Pagination page);

	/**
	 * 
	 * @param id 关键字ID
	 * @return
	 * TODO 启用 关闭 关键字
	 */
	CommonResult enableKeyword(Integer id);

	/**
	 * 
	 * @param keyword 更改后的关键字
	 * @return
	 * TODO 更新关键字
	 */
	CommonResult updateKeyword(Roomkeyword keyword);

	/**
	 * 
	 * @param belong 
	 * @param file 关键字excel
	 * @return
	 * TODO 上传excel导入关键字
	 */
	CommonResult uploadExcel(Integer belong, MultipartFile file);

	/**
	 * 
	 * @param id 关键字ID
	 * @return
	 * TODO 删除关键字
	 */
	CommonResult deleteKeyword(Integer id);

	/**
	 * 
	 * @param path 
	 * @param belong 角色分类  1  gemii  2  wyeth
	 * @return
	 * TODO 导出全局关键词
	 */
	Map<String, Object> exportExcel(String path, Integer belong);
}
