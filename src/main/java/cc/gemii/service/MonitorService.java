package cc.gemii.service;

import cc.gemii.pojo.CommonResult;

public interface MonitorService {

	/**
	 * 
	 * @param mID 班长ID
	 * @return
	 * TODO 获取班长id为mID的群列表,并根据robottag区分
	 */
	CommonResult getRoomsByMid(Integer mID);
}
