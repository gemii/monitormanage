package cc.gemii.service;

import cc.gemii.pojo.CommonResult;

public interface AdminService {

	CommonResult login(String userName, String password, Integer type);

}
