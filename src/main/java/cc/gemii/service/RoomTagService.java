package cc.gemii.service;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;

public interface RoomTagService {

	/**
	 * 
	 * @param roomIDs 群ID数组
	 * @return
	 * TODO 验证群ID
	 */
	CommonResult validateRoomIDs(String roomIDs);

	/**
	 * 
	 * @param search 查询字符
	 * @param belong 1gemii 2 wyeth
	 * @param page 分页包装类
	 * @return
	 * TODO 获取全局群标签
	 */
	CommonResult getGlobalRoomTags(String search, Integer belong, Pagination page);

	/**
	 * 
	 * @param oldTag 修改前的标签名
	 * @param newTag 修改后的标签名
	 * @param roomIDs 匹配群数组
	 * @param action update:更新  insert:插入 
	 * @param belong 
	 * @return
	 * TODO
	 */
	CommonResult insertOrUpdateRoomTags(String oldTag, String newTag, String[] roomIDs,
			String action, Integer belong);

	/**
	 * 
	 * @param tagName 标签名
	 * @param belong 
	 * @return
	 * TODO 删除标签
	 */
	CommonResult deleteRoomTag(String tagName, Integer belong);

	/**
	 * 
	 * @param tagName 获取群标签详细信息
	 * @param belong 
	 * @return
	 * TODO
	 */
	CommonResult getRoomTag(String tagName, Integer belong);

}
