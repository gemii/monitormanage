package cc.gemii.service;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Knowledge;
import cc.gemii.pojo.Pagination;

public interface KnowledgeService {

	/**
	 * 
	 * @param id 问题ID
	 * @param question 问题内容
	 * @param belong 1 gemii 2 wyeth
	 * @param page 分页包装类
	 * @return
	 * TODO
	 */
	CommonResult getKnowledgeList(Integer id, String question, Integer belong, Pagination page);

	/**
	 * 
	 * @param knowledge 问题包装类
	 * @return
	 * TODO
	 */
	CommonResult insertKnowledge(Knowledge knowledge);

	/**
	 * 
	 * @param belong 
	 * @param file
	 * @param response
	 * @return
	 * TODO 上传excel导入知识库
	 */
	CommonResult insertKnowledgeFromExcel(Integer belong, MultipartFile file);

	/**
	 * 
	 * @param id 问题ID
	 * @return
	 * TODO 获取某问题详细信息
	 */
	CommonResult getKnowledge(Integer id);

	/**
	 * 
	 * @param knowledge 问题包装类
	 * @return
	 * TODO 更新问题或此问题答案
	 */
	CommonResult updateKnowledge(Knowledge knowledge);

	/**
	 * 
	 * @param id 问题ID
	 * @return
	 * TODO 删除问题
	 */
	CommonResult deleteKnowledge(Integer id);

	/**
	 * 
	 * @param belong 角色分类  1  gemii  2  wyeth
	 * @return
	 * TODO 导出模板消息
	 */
	Map<String, Object> exportExcel(String path, Integer belong);

}
