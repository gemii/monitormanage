package cc.gemii.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.mapper.RoomkeywordMapper;
import cc.gemii.mapper.custom.KeywordMapper;
import cc.gemii.po.Roomkeyword;
import cc.gemii.po.RoomkeywordExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.KeywordService;
import cc.gemii.util.ExcelUtil;
import cc.gemii.util.ExportExcel;
import cc.gemii.util.TimeUtils;

@Service
public class KeywordServiceImpl implements KeywordService{

	@Autowired
	private RoomkeywordMapper keywordMapper;
	
	@Autowired
	private KeywordMapper keywordCustomMapper;
	
	@Override
	public CommonResult insertGlobalKeyword(Roomkeyword roomKeyword) {
		RoomkeywordExample kExample = new RoomkeywordExample();
		kExample.createCriteria()
			.andKeywordEqualTo(roomKeyword.getKeyword())
			.andBelongEqualTo(roomKeyword.getBelong())
			.andTypeEqualTo(1);
		List<Roomkeyword> keywords = keywordMapper.selectByExample(kExample);
		if(keywords.size() > 0){
			return CommonResult.build(-1, "此全局关键字已存在");
		}
		roomKeyword.setCtime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		roomKeyword.setType(1);
		roomKeyword.setStatus("开启");
		keywordMapper.insert(roomKeyword);
		return CommonResult.ok();
	}

	@Override
	public CommonResult getGlobalKeyword(Integer id, String keyword, Integer belong, Pagination page) {
		Map<String, Object> param = new HashMap<String, Object>();
		if(id != null){
			param.put("id", id);
		}
		if(!"".equals(keyword) && keyword != null){
			param.put("keyword", "%" + keyword + "%");
		}
		param.put("start", (page.getPage() - 1) * page.getPageSize());
		param.put("end", page.getPageSize());
		param.put("belong", belong);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("data", keywordCustomMapper.getGlobalKeyword(param));
		response.put("totalPage", Pagination.computePages(keywordCustomMapper.getGlobalKeywordCount(param), page.getPageSize()));
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult enableKeyword(Integer id) {
		keywordCustomMapper.enableKeyword(id);
		return CommonResult.ok();
	}

	@Override
	public CommonResult updateKeyword(Roomkeyword keyword) {
		keywordMapper.updateByPrimaryKeySelective(keyword);
		return CommonResult.ok();
	}

	@Override
	public CommonResult uploadExcel(Integer belong, MultipartFile file) {
		try {
			System.out.println(file);
			List<String[]> list = ExcelUtil.parseUserUploadExcel(file.getInputStream());
			List<String> keywords = new ArrayList<String>();
			for (String[] keyword : list) {
				keywords.add(keyword[0]);
			}
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("belong", belong);
			param.put("keywords", keywords);
			keywordCustomMapper.insertKeywordFromExcel(param);
			return CommonResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return CommonResult.build(-1, "excel error");
		}
		
	}

	@Override
	public CommonResult deleteKeyword(Integer id) {
		keywordMapper.deleteByPrimaryKey(id);
		return CommonResult.ok();
	}

	@Override
	public Map<String, Object> exportExcel(String path, Integer belong) {
		//导出excel准备
		ExportExcel<List<ArrayList<String[]>>> export = new ExportExcel<List<ArrayList<String[]>>>();
		String fileName = "全局关键词.xlsx";
		String[] sheetnames = {"全局关键词"};
		List<ArrayList<String[]>> list = new ArrayList<ArrayList<String[]>>();
		
		//参数包装
		try {
			List<String> records  = keywordCustomMapper.getAllGlobalKeyword(belong);
			
			ArrayList<String[]> rows = new ArrayList<String[]>();
			String[] record_title = new String[]{"关键词"};
			rows.add(record_title);
			String[] record_each = null;
			for (String keyword : records) {
				record_each = new String[]{keyword};
				rows.add(record_each);
			}
			list.add(rows);
			export.write_Excel(path + fileName, list, sheetnames);
			
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("fileName", fileName);
			response.put("file", new File(path + fileName));
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}


