package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.MembertagbindMapper;
import cc.gemii.mapper.WechatroommemberinfoMapper;
import cc.gemii.mapper.custom.MemberMapper;
import cc.gemii.po.Membertagbind;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService{

	@Autowired
	private WechatroommemberinfoMapper roomMemberMapper;
	
	@Autowired
	private MemberMapper memberMapper;
	
	@Autowired
	private MembertagbindMapper bindMapper;
	
	@Override
	public CommonResult selectMember(String displayName, String nickname) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("displayName", displayName == null? "%%" : "%" + displayName + "%");
		param.put("nickname", nickname == null? "%%" : "%" + nickname + "%");
		return CommonResult.ok(memberMapper.selectMember(param));
	}

	@Override
	public CommonResult bindTag(String memberID, Integer tagID) {
		Membertagbind bindTag = new Membertagbind();
		bindTag.setMemberid(memberID);
		bindTag.setTagid(tagID);
		bindMapper.insert(bindTag);
		return CommonResult.ok();
	}

}
