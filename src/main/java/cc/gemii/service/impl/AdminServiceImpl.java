package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.custom.AdminMapper;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.User;
import cc.gemii.service.AdminService;
import cc.gemii.util.MD5Utils;

@Service
public class AdminServiceImpl implements AdminService{

	@Autowired
	private AdminMapper adminMapper;

	@Override
	public CommonResult login(String userName, String password, Integer type) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userName", userName);
		param.put("password", MD5Utils.md5Encode(password));
		param.put("type", type);
		User user = adminMapper.login(param);
		if(user == null){
			return CommonResult.build(-1, "账户名或密码错误");
		}
		return CommonResult.ok(user);
	}

}
