package cc.gemii.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.mapper.KnowledgeanswerMapper;
import cc.gemii.mapper.KnowledgequestionMapper;
import cc.gemii.mapper.custom.KnowledgeMapper;
import cc.gemii.po.Knowledgeanswer;
import cc.gemii.po.Knowledgequestion;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Knowledge;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.KnowledgeService;
import cc.gemii.util.ExcelUtil;
import cc.gemii.util.ExportExcel;

@Service
public class KnowledgeServiceImpl implements KnowledgeService{

	@Autowired
	private KnowledgeMapper knowledgeMapper;
	
	@Autowired
	private KnowledgequestionMapper questionMapper;
	
	@Autowired
	private KnowledgeanswerMapper answerMapper;
	
	@Override
	public CommonResult getKnowledgeList(Integer id, String question, Integer belong, Pagination page) {
		Map<String, Object> param = new HashMap<String, Object>();
		if(id != null){
			param.put("id", id);
		}
		if(!"".equals(question) && question != null){
			param.put("question", "%" + question + "%");
		}
		param.put("start", (page.getPage() - 1) * page.getPageSize());
		param.put("end", page.getPageSize());
		param.put("belong", belong);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("data", knowledgeMapper.getKnowledgeList(param));
		response.put("totalPage", Pagination.computePages(knowledgeMapper.getKnowledgeListCount(param), page.getPageSize()));
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult insertKnowledge(Knowledge knowledge) {
		insert(knowledge.getQuestion(), knowledge.getAnswer(), knowledge.getBelong());
		return CommonResult.ok();
	}

	private void insert(String question, String answer, Integer belong){
		Knowledgequestion knowledgequestion = new Knowledgequestion();
		knowledgequestion.setQuestion(question);
		knowledgequestion.setBelong(belong);
		questionMapper.insert(knowledgequestion);
		Knowledgeanswer knowledgeanswer = new Knowledgeanswer();
		knowledgeanswer.setContent(answer);
		knowledgeanswer.setQuestionid(knowledgequestion.getId());
		answerMapper.insert(knowledgeanswer);
	}

	@Override
	public CommonResult insertKnowledgeFromExcel(Integer belong, MultipartFile file) {
		try {
			List<String[]> list = ExcelUtil.parseUserUploadExcel(file.getInputStream());
			for (String[] knowledge : list) {
				insert(knowledge[0], knowledge[1], belong);
			}
			return CommonResult.ok();
		}catch (StringIndexOutOfBoundsException exception){
			return CommonResult.build(-1, "excel error");
		}catch (Exception e) {
			return CommonResult.build(500, "server error");
		}
	}

	@Override
	public CommonResult getKnowledge(Integer id) {
		return CommonResult.ok(knowledgeMapper.getKnowledge(id));
	}

	@Override
	public CommonResult updateKnowledge(Knowledge knowledge) {
		knowledgeMapper.updateKnowledge(knowledge);
		return CommonResult.ok();
	}

	@Override
	public CommonResult deleteKnowledge(Integer id) {
		knowledgeMapper.deleteKnowledge(id);
		return CommonResult.ok();
	}

	@Override
	public Map<String, Object> exportExcel(String path, Integer belong) {
		//导出excel准备
		ExportExcel<List<ArrayList<String[]>>> export = new ExportExcel<List<ArrayList<String[]>>>();
		String fileName = "模板消息.xlsx";
		String[] sheetnames = {"模板消息"};
		List<ArrayList<String[]>> list = new ArrayList<ArrayList<String[]>>();
		
		//参数包装
		try {
			List<Knowledge> records  = knowledgeMapper.getAllKnowledge(belong);
			
			ArrayList<String[]> rows = new ArrayList<String[]>();
			String[] record_title = new String[]{"问题", "答案"};
			rows.add(record_title);
			String[] record_each = null;
			for (Knowledge knowledge : records) {
				record_each = new String[]{knowledge.getQuestion(), knowledge.getAnswer()};
				rows.add(record_each);
			}
			list.add(rows);
			export.write_Excel(path + fileName, list, sheetnames);
			
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("fileName", fileName);
			response.put("file", new File(path + fileName));
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
