package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.MembertagMapper;
import cc.gemii.mapper.custom.MemberTagMapper;
import cc.gemii.po.Membertag;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.MemberTagService;

@Service
public class MemberTagServiceImpl implements MemberTagService{

	@Autowired
	private MembertagMapper tagMapper;
	
	@Autowired
	private MemberTagMapper tagCustomMapper;
	
	@Override
	public CommonResult getTagList(Integer id, String search, Integer belong, Pagination page) {
		Map<String, Object> param = new HashMap<String, Object>();
		if(id != null){
			param.put("id", id);
		}
		if(!"".equals(search) && search != null){
			param.put("tag", "%" + search + "%");
		}
		param.put("start", (page.getPage() - 1) * page.getPageSize());
		param.put("end", page.getPageSize());
		param.put("belong", belong);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("data", tagCustomMapper.getTagList(param));
		response.put("totalPage", Pagination.computePages(tagCustomMapper.getTagListCount(param), page.getPageSize()));
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult addTag(Membertag memberTag) {
		tagMapper.insert(memberTag);
		return CommonResult.ok();
	}

	@Override
	public CommonResult deleteTag(Integer id) {
		tagCustomMapper.deleteTag(id);
		return CommonResult.ok();
	}

}
