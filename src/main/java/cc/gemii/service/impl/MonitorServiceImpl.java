package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.MonitorrobotbindMapper;
import cc.gemii.mapper.custom.MonitorCustomMapper;
import cc.gemii.po.Monitorrobotbind;
import cc.gemii.po.MonitorrobotbindExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MonitorService;

@Service
public class MonitorServiceImpl implements MonitorService{

	@Autowired
	private MonitorrobotbindMapper mrBindMapper;
	
	@Autowired
	private MonitorCustomMapper monitorCustomMapper;
	
	@Override
	public CommonResult getRoomsByMid(Integer mID) {
		MonitorrobotbindExample bExample = new MonitorrobotbindExample();
		bExample.createCriteria()
			.andMonitoridEqualTo(mID);
		List<Monitorrobotbind> binds = mrBindMapper.selectByExample(bExample);
		Map<String, Object> param = null;
		List<Map<String, Object>> responseList = new ArrayList<Map<String,Object>>();
		Map<String, Object> responseMap = null;
		for (Monitorrobotbind bind : binds) {
			param = new HashMap<String, Object>();
			param.put("mID", mID);
			param.put("tag", bind.getRobottag());
			responseMap = new HashMap<String, Object>();
			responseMap.put("main", bind);
			responseMap.put("rooms", monitorCustomMapper.getRoomsBySendRobot(param));
			responseList.add(responseMap);
		}
		return CommonResult.ok(responseList);
	}

}
