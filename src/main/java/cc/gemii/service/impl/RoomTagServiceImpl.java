package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.custom.RoomTagMapper;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.RoomTagService;

@Service
public class RoomTagServiceImpl implements RoomTagService{

	@Autowired
	private RoomTagMapper tagMapper;
	
	@Override
	public CommonResult validateRoomIDs(String roomIDs) {
		List<String> roomIDList = new ArrayList<String>(Arrays.asList(roomIDs.split(",")));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("roomIDs", roomIDList);
		List<String> existsRoomIDs = tagMapper.validateRoomIDs(param);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("total", roomIDList.size());
		response.put("success", existsRoomIDs.size());
		roomIDList.removeAll(existsRoomIDs);
		response.put("failed", roomIDList.size());
		response.put("failedRooms", roomIDList);
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult getGlobalRoomTags(String search, Integer belong, Pagination page) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("search", "%" + search + "%");
		param.put("start", (page.getPage() - 1) * page.getPageSize());
		param.put("end", page.getPageSize());
		param.put("belong", belong);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("data", tagMapper.getGlobalRoomTags(param));
		response.put("totalPage", Pagination.computePages(tagMapper.getGlobalRoomTagsCount(param), page.getPageSize()));
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult insertOrUpdateRoomTags(String oldTag, String newTag, String[] roomIDs, String action, Integer belong) {
		if(roomIDs.length == 0){
			return CommonResult.build(-1, "roomIDs null");
		}
		if("update".equals(action)){            //更新
			return this.updateRoomTags(oldTag, newTag, roomIDs, belong);
		}else if("insert".equals(action)){      //插入
			return this.insertRoomTags(newTag, roomIDs, belong);
		}else {
			return CommonResult.build(-2, "action error");
		}
	}

	private CommonResult insertRoomTags(String newTag, String[] roomIDs, Integer belong) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("newTag", newTag);
		param.put("roomIDs", roomIDs);
		param.put("belong", belong);
		tagMapper.insertRoomTag(param);
		return CommonResult.ok();
	}

	private CommonResult updateRoomTags(String oldTag, String newTag, String[] roomIDs, Integer belong) {
		deleteRoomTag(oldTag, belong);
		insertRoomTags(newTag, roomIDs, belong);
		return CommonResult.ok();
	}

	@Override
	public CommonResult deleteRoomTag(String tagName, Integer belong) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tagName", tagName);
		param.put("belong", belong);
		tagMapper.deleteRoomTag(param);
		return CommonResult.ok();
	}

	@Override
	public CommonResult getRoomTag(String tagName, Integer belong) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tagName", tagName);
		param.put("belong", belong);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("tagName", tagName);
		response.put("roomIDs", tagMapper.getRoomTag(param));
		return CommonResult.ok(response);
	}
	
}
