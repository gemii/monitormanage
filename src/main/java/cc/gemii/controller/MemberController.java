package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MemberService;

@Controller
@RequestMapping("/member")
public class MemberController {

	@Autowired
	private MemberService memberService;
	
	/**
	 * @param displayName 群内昵称
	 * @param nickname 微信昵称
	 * TODO 根据群内昵称,微信昵称模糊查询用户信息
	 */
	@RequestMapping("/selectMember")
	@ResponseBody
	public CommonResult selectMember(@RequestParam(required=false) String displayName,
			@RequestParam(required=false) String nickname,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return memberService.selectMember(displayName, nickname);
	}
	
	/**
	 * @param memberID 用户ID
	 * @param tagID 标签ID
	 * TODO 给用户打全局标签
	 */
	@RequestMapping("/bindTag")
	@ResponseBody
	public CommonResult bindTag(@RequestParam String memberID, 
			@RequestParam Integer tagID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return memberService.bindTag(memberID, tagID);
	}
}
