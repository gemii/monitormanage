package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.AdminService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;
	
	@RequestMapping("/login")
	@ResponseBody
	public CommonResult login(@RequestParam String userName,
			@RequestParam String password,
			@RequestParam Integer type,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return adminService.login(userName, password, type);
	}
}
