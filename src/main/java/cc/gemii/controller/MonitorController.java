package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MonitorService;

@Controller
@RequestMapping("/monitor")
public class MonitorController {

	@Autowired
	private MonitorService monitorService;
	
	/**
	 * @param mID 班长ID
	 * TODO 根据mID查询对应的群,并根据tag区分
	 */
	@RequestMapping("/getRoomsByMid")
	@ResponseBody
	public CommonResult getRoomsByMonitorID(@RequestParam Integer mID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.getRoomsByMid(mID);
	}
}
