package cc.gemii.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Knowledge;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.KnowledgeService;

@Controller
@RequestMapping("/knowledge")
public class KnowledgeController {

	@Autowired
	private KnowledgeService knowledgeService;
	
	/**
	 * 
	 * @param id 问题ID
	 * @param question 问题内容
	 * @param page 分页包装类
	 * @return
	 * TODO
	 */
	@RequestMapping("/list")
	@ResponseBody
	public CommonResult getKnowledgeList(@RequestParam(required=false) Integer id,
			@RequestParam(required=false) String question,
			@RequestParam Integer belong,
			Pagination page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return knowledgeService.getKnowledgeList(id, question, belong, page);
	}
	
	/**
	 * 
	 * @param knowledge 问题包装类
	 * @return
	 * TODO
	 */
	@RequestMapping("/insert")
	@ResponseBody
	public CommonResult insertKnowledge(Knowledge knowledge,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return knowledgeService.insertKnowledge(knowledge);
	}
	
	/**
	 * 
	 * @param file
	 * @param response
	 * @return
	 * TODO 上传excel导入知识库
	 */
	@RequestMapping("/uploadExcel")
	@ResponseBody
	public CommonResult insertKnowledgeFromExcel(MultipartFile file,
			@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return knowledgeService.insertKnowledgeFromExcel(belong, file);
	}

	/**
	 * 
	 * @param id 问题ID
	 * @return
	 * TODO 获取某问题详细信息
	 */
	@RequestMapping("/get")
	@ResponseBody
	public CommonResult getKnowledge(@RequestParam Integer id,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return knowledgeService.getKnowledge(id);
	}
	
	/**
	 * 
	 * @param knowledge 问题包装类
	 * @return
	 * TODO 更新问题或此问题答案
	 */
	@RequestMapping("/update")
	@ResponseBody
	public CommonResult updateKnowledge(Knowledge knowledge,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return knowledgeService.updateKnowledge(knowledge);
	}
	
	/**
	 * 
	 * @param id 问题ID
	 * @return
	 * TODO 删除问题
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public CommonResult deleteKnowledge(@RequestParam Integer id,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return knowledgeService.deleteKnowledge(id);
	}

	/**
	 * 
	 * @param belong 角色分类  1  gemii  2  wyeth
	 * @return
	 * TODO 导出模板消息
	 */
	@RequestMapping("/export")
	@ResponseBody
	public Object exportExcel(@RequestParam Integer belong,
			HttpServletRequest request,
			HttpServletResponse response){
		try {
			response.setHeader("Access-Control-Allow-Origin", "*");
			HttpHeaders headers = new HttpHeaders();  
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			String path = request.getSession().getServletContext().getRealPath("") + "file/";
			Map<String, Object> result = knowledgeService.exportExcel(path, belong);
			if(result == null){
				System.out.println("123");
				return CommonResult.build(-1, "error");
			}
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(String.valueOf(result.get("fileName")),"UTF-8")); 
			return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray((File) result.get("file")),  headers, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return CommonResult.build(-1, "error");
		}
	}
}
