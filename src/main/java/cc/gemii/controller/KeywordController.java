package cc.gemii.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.po.Roomkeyword;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.KeywordService;

@Controller
@RequestMapping("/keyword")
public class KeywordController {

	@Autowired
	private KeywordService keywordService;
	
	/**
	 * @param roomKeyword 全局关键字包装类
	 * TODO 新添全局关键字
	 */
	@RequestMapping("/insert")
	@ResponseBody
	public CommonResult insertGlobalKeyword(Roomkeyword roomKeyword,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.insertGlobalKeyword(roomKeyword);
	}
	
	/**
	 * TODO 获取全局关键字列表
	 */
	@RequestMapping("/list")
	@ResponseBody
	public CommonResult getGlobalKeyword(@RequestParam(required=false) Integer id,
			@RequestParam(required=false) String keyword,
			@RequestParam Integer belong,
			Pagination page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.getGlobalKeyword(id, keyword, belong, page);
	}
	
	/**
	 * 
	 * @param id 关键字ID
	 * @return
	 * TODO 启用 关闭 关键字
	 */
	@RequestMapping("/enable")
	@ResponseBody
	public CommonResult enableKeyword(@RequestParam Integer id,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.enableKeyword(id);
	}
	
	/**
	 * 
	 * @param keyword 更改后的关键字包装类
	 * @return
	 * TODO 更新关键字
	 */
	@RequestMapping("/update")
	@ResponseBody
	public CommonResult updateKeyword(Roomkeyword keyword,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.updateKeyword(keyword);
	}
	
	/**
	 * 
	 * @param file 关键字excel
	 * @return
	 * TODO 上传excel导入关键字
	 */
	@RequestMapping("/uploadExcel")
	@ResponseBody
	public CommonResult uploadExcel(MultipartFile file,
			@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.uploadExcel(belong, file);
	}
	
	/**
	 * 
	 * @param id 关键字ID
	 * @return
	 * TODO 删除关键字
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public CommonResult deleteKeyword(@RequestParam Integer id,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.deleteKeyword(id);
	}
	
	/**
	 * 
	 * @param belong 角色分类  1  gemii  2  wyeth
	 * @return
	 * TODO 导出全局关键词
	 */
	@RequestMapping("/export")
	@ResponseBody
	public Object exportExcel(@RequestParam Integer belong,
			HttpServletRequest request,
			HttpServletResponse response){
		try {
			response.setHeader("Access-Control-Allow-Origin", "*");
			HttpHeaders headers = new HttpHeaders();  
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			String path = request.getSession().getServletContext().getRealPath("") + "file/";
			Map<String, Object> result = keywordService.exportExcel(path, belong);
			if(result == null){
				System.out.println("123");
				return CommonResult.build(-1, "error");
			}
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(String.valueOf(result.get("fileName")),"UTF-8")); 
			return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray((File) result.get("file")),  headers, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return CommonResult.build(-1, "error");
		}
	}
}
