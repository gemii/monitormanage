package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Membertag;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.MemberTagService;

@Controller
@RequestMapping("/mtag")
public class MemberTagController {

	@Autowired
	private MemberTagService tagService;
	
	/**
	 * 
	 * TODO 获取标签列表
	 */
	@RequestMapping("/list")
	@ResponseBody
	public CommonResult getTagList(@RequestParam(required=false) Integer id,
			@RequestParam(required=false) String search,
			@RequestParam Integer belong,
			Pagination page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return tagService.getTagList(id, search, belong, page);
	}
	
	/**
	 * 
	 * @param memberTag tag包装类
	 * TODO 新添标签
	 */
	@RequestMapping("/add")
	@ResponseBody
	public CommonResult addTag(Membertag memberTag,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return tagService.addTag(memberTag);
	}

	/**
	 * 
	 * @param id 标签ID
	 * @return
	 * TODO 删除标签
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public CommonResult delete(@RequestParam Integer id,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return tagService.deleteTag(id);
	}
	
}
