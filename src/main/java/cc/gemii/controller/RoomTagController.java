package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.RoomTagService;

@Controller
@RequestMapping("/rtag")
public class RoomTagController {

	@Autowired
	private RoomTagService tagService;
	
	/**
	 * 
	 * @param roomIDs 群ID数组
	 * @return
	 * TODO 验证群ID
	 */
	@RequestMapping("/validateRoomIDs")
	@ResponseBody
	public CommonResult validateRoomIDs(String roomIDs,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return tagService.validateRoomIDs(roomIDs);
	}
	
	/**
	 * 
	 * @param search 查询字符
	 * @param page 分页包装类
	 * @return
	 * TODO 获取全局群标签
	 */
	@RequestMapping("/list")
	@ResponseBody
	public CommonResult getGlobalRoomTags(@RequestParam String search,
			@RequestParam Integer belong,
			Pagination page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return tagService.getGlobalRoomTags(search, belong, page);
	}
	
	/**
	 * @param oldTag 修改前的标签名
	 * @param newTag 修改后的标签名
	 * @param roomIDs 匹配群数组
	 * @param action update:更新  insert:插入 
	 * @return
	 * TODO
	 */
	@RequestMapping("/insertOrUpdate")
	@ResponseBody
	public CommonResult insertOrUpdateRoomTags(@RequestParam(required=false) String oldTag,
			@RequestParam(required=false) String newTag,
			@RequestParam String[] roomIDs,
			@RequestParam String action,
			@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return tagService.insertOrUpdateRoomTags(oldTag, newTag, roomIDs, action, belong);
	}
	
	/**
	 * 
	 * @param tagName 标签名
	 * @return
	 * TODO 删除标签
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public CommonResult deleteRoomTag(@RequestParam String tagName,
			@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return tagService.deleteRoomTag(tagName, belong);
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public CommonResult getRoomTag(@RequestParam String tagName,
			@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		return tagService.getRoomTag(tagName, belong);
	}
}
