package cc.gemii.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

	public static List<String[]> parseUserUploadExcel(InputStream is) {
        List<String[]> list = new ArrayList<String[]>();
        try {
            //POIFSFileSystem pois = new POIFSFileSystem(file.getInputStream());
            //新建WorkBook
            XSSFWorkbook wb = new XSSFWorkbook(is);
            //获取Sheet（工作薄）总个数
            int sheetNumber = wb.getNumberOfSheets();
            for (int i = 0; i < sheetNumber; i++) {
                //获取Sheet（工作薄）
                XSSFSheet sheet = wb.getSheetAt(i);
                //开始行数
                int firstRow = sheet.getFirstRowNum();
                //结束行数
                int lastRow = sheet.getLastRowNum();
                //判断该Sheet（工作薄)是否为空
                boolean isEmpty = false;
                if(firstRow == lastRow){
                    isEmpty = true;
                }
                if(!isEmpty){
                    for (int j = firstRow+1; j <= lastRow; j++) {
                        //获取一行
                        XSSFRow row = sheet.getRow(j);
                        //开始列数
                        int firstCell = row.getFirstCellNum();
                        //结束列数
                        int lastCell = row.getLastCellNum();
                        //判断该行是否为空
                        String[] value = new String[lastCell]; 
                        if(firstCell != lastCell){
                            for (int k = firstCell; k < lastCell; k++) {
                                //获取一个单元格
                                XSSFCell cell = row.getCell(k);
                                Object str = null;
                                //获取单元格，值的类型
                                int cellType = cell.getCellType();
                                
                                if(cellType == 0){
                                    str = cell.getNumericCellValue();
                                }else if(cellType == 1){
                                    str = cell.getStringCellValue();
                                }else if(cellType == 2){
                                }else if(cellType == 4){
                                    str = cell.getBooleanCellValue();
                                }
                                System.out.println(str);
                              value[k] = (String) str;
                            }
                             
                        }
                        //每一行循环完对应的就是一个用户故事的所有属性全部拿到
                        list.add(value);  
                    }
                     
                }
                 
            }
        } catch (IOException e) {
            
            e.printStackTrace();
        }
        return list;
    }
	
}
