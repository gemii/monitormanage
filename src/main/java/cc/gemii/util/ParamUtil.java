package cc.gemii.util;

public class ParamUtil {

	//add robot url
	public static final String START_ROBOT_URL = "http://helper.gemii.cc:8082/cmd?act=start&bot=BOT&new=NEW";
	
	//get robot tag
	public static final String GET_ROBOT_TAG_URL = "http://wyeth.gemii.cc/HelperManage/user/getTag";
	
	//add robot response : start success
	public static final String START_ROBOT_SUCCESS = "0";
	
	//redis : login
	public static final String ITEM_LOGIN = "login";
	
	//not found QR code url
	public static final String NOT_FOUND_QRCODE = "未获取到二维码URL";
	
	//redis NextTag
	public static final String ROBOT_NEXT_TAG = "NextTag";
}
