package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class WechatroommemberinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WechatroommemberinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMemberidIsNull() {
            addCriterion("MemberID is null");
            return (Criteria) this;
        }

        public Criteria andMemberidIsNotNull() {
            addCriterion("MemberID is not null");
            return (Criteria) this;
        }

        public Criteria andMemberidEqualTo(String value) {
            addCriterion("MemberID =", value, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidNotEqualTo(String value) {
            addCriterion("MemberID <>", value, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidGreaterThan(String value) {
            addCriterion("MemberID >", value, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidGreaterThanOrEqualTo(String value) {
            addCriterion("MemberID >=", value, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidLessThan(String value) {
            addCriterion("MemberID <", value, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidLessThanOrEqualTo(String value) {
            addCriterion("MemberID <=", value, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidLike(String value) {
            addCriterion("MemberID like", value, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidNotLike(String value) {
            addCriterion("MemberID not like", value, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidIn(List<String> values) {
            addCriterion("MemberID in", values, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidNotIn(List<String> values) {
            addCriterion("MemberID not in", values, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidBetween(String value1, String value2) {
            addCriterion("MemberID between", value1, value2, "memberid");
            return (Criteria) this;
        }

        public Criteria andMemberidNotBetween(String value1, String value2) {
            addCriterion("MemberID not between", value1, value2, "memberid");
            return (Criteria) this;
        }

        public Criteria andRoomidIsNull() {
            addCriterion("RoomID is null");
            return (Criteria) this;
        }

        public Criteria andRoomidIsNotNull() {
            addCriterion("RoomID is not null");
            return (Criteria) this;
        }

        public Criteria andRoomidEqualTo(String value) {
            addCriterion("RoomID =", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotEqualTo(String value) {
            addCriterion("RoomID <>", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidGreaterThan(String value) {
            addCriterion("RoomID >", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidGreaterThanOrEqualTo(String value) {
            addCriterion("RoomID >=", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLessThan(String value) {
            addCriterion("RoomID <", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLessThanOrEqualTo(String value) {
            addCriterion("RoomID <=", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLike(String value) {
            addCriterion("RoomID like", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotLike(String value) {
            addCriterion("RoomID not like", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidIn(List<String> values) {
            addCriterion("RoomID in", values, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotIn(List<String> values) {
            addCriterion("RoomID not in", values, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidBetween(String value1, String value2) {
            addCriterion("RoomID between", value1, value2, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotBetween(String value1, String value2) {
            addCriterion("RoomID not between", value1, value2, "roomid");
            return (Criteria) this;
        }

        public Criteria andUinIsNull() {
            addCriterion("Uin is null");
            return (Criteria) this;
        }

        public Criteria andUinIsNotNull() {
            addCriterion("Uin is not null");
            return (Criteria) this;
        }

        public Criteria andUinEqualTo(String value) {
            addCriterion("Uin =", value, "uin");
            return (Criteria) this;
        }

        public Criteria andUinNotEqualTo(String value) {
            addCriterion("Uin <>", value, "uin");
            return (Criteria) this;
        }

        public Criteria andUinGreaterThan(String value) {
            addCriterion("Uin >", value, "uin");
            return (Criteria) this;
        }

        public Criteria andUinGreaterThanOrEqualTo(String value) {
            addCriterion("Uin >=", value, "uin");
            return (Criteria) this;
        }

        public Criteria andUinLessThan(String value) {
            addCriterion("Uin <", value, "uin");
            return (Criteria) this;
        }

        public Criteria andUinLessThanOrEqualTo(String value) {
            addCriterion("Uin <=", value, "uin");
            return (Criteria) this;
        }

        public Criteria andUinLike(String value) {
            addCriterion("Uin like", value, "uin");
            return (Criteria) this;
        }

        public Criteria andUinNotLike(String value) {
            addCriterion("Uin not like", value, "uin");
            return (Criteria) this;
        }

        public Criteria andUinIn(List<String> values) {
            addCriterion("Uin in", values, "uin");
            return (Criteria) this;
        }

        public Criteria andUinNotIn(List<String> values) {
            addCriterion("Uin not in", values, "uin");
            return (Criteria) this;
        }

        public Criteria andUinBetween(String value1, String value2) {
            addCriterion("Uin between", value1, value2, "uin");
            return (Criteria) this;
        }

        public Criteria andUinNotBetween(String value1, String value2) {
            addCriterion("Uin not between", value1, value2, "uin");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNull() {
            addCriterion("NickName is null");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNotNull() {
            addCriterion("NickName is not null");
            return (Criteria) this;
        }

        public Criteria andNicknameEqualTo(String value) {
            addCriterion("NickName =", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotEqualTo(String value) {
            addCriterion("NickName <>", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThan(String value) {
            addCriterion("NickName >", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThanOrEqualTo(String value) {
            addCriterion("NickName >=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThan(String value) {
            addCriterion("NickName <", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThanOrEqualTo(String value) {
            addCriterion("NickName <=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLike(String value) {
            addCriterion("NickName like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotLike(String value) {
            addCriterion("NickName not like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameIn(List<String> values) {
            addCriterion("NickName in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotIn(List<String> values) {
            addCriterion("NickName not in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameBetween(String value1, String value2) {
            addCriterion("NickName between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotBetween(String value1, String value2) {
            addCriterion("NickName not between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andAttrstatusIsNull() {
            addCriterion("AttrStatus is null");
            return (Criteria) this;
        }

        public Criteria andAttrstatusIsNotNull() {
            addCriterion("AttrStatus is not null");
            return (Criteria) this;
        }

        public Criteria andAttrstatusEqualTo(String value) {
            addCriterion("AttrStatus =", value, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusNotEqualTo(String value) {
            addCriterion("AttrStatus <>", value, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusGreaterThan(String value) {
            addCriterion("AttrStatus >", value, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusGreaterThanOrEqualTo(String value) {
            addCriterion("AttrStatus >=", value, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusLessThan(String value) {
            addCriterion("AttrStatus <", value, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusLessThanOrEqualTo(String value) {
            addCriterion("AttrStatus <=", value, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusLike(String value) {
            addCriterion("AttrStatus like", value, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusNotLike(String value) {
            addCriterion("AttrStatus not like", value, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusIn(List<String> values) {
            addCriterion("AttrStatus in", values, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusNotIn(List<String> values) {
            addCriterion("AttrStatus not in", values, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusBetween(String value1, String value2) {
            addCriterion("AttrStatus between", value1, value2, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andAttrstatusNotBetween(String value1, String value2) {
            addCriterion("AttrStatus not between", value1, value2, "attrstatus");
            return (Criteria) this;
        }

        public Criteria andPyinitialIsNull() {
            addCriterion("PYInitial is null");
            return (Criteria) this;
        }

        public Criteria andPyinitialIsNotNull() {
            addCriterion("PYInitial is not null");
            return (Criteria) this;
        }

        public Criteria andPyinitialEqualTo(String value) {
            addCriterion("PYInitial =", value, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialNotEqualTo(String value) {
            addCriterion("PYInitial <>", value, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialGreaterThan(String value) {
            addCriterion("PYInitial >", value, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialGreaterThanOrEqualTo(String value) {
            addCriterion("PYInitial >=", value, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialLessThan(String value) {
            addCriterion("PYInitial <", value, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialLessThanOrEqualTo(String value) {
            addCriterion("PYInitial <=", value, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialLike(String value) {
            addCriterion("PYInitial like", value, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialNotLike(String value) {
            addCriterion("PYInitial not like", value, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialIn(List<String> values) {
            addCriterion("PYInitial in", values, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialNotIn(List<String> values) {
            addCriterion("PYInitial not in", values, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialBetween(String value1, String value2) {
            addCriterion("PYInitial between", value1, value2, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyinitialNotBetween(String value1, String value2) {
            addCriterion("PYInitial not between", value1, value2, "pyinitial");
            return (Criteria) this;
        }

        public Criteria andPyquanpinIsNull() {
            addCriterion("PYQuanPin is null");
            return (Criteria) this;
        }

        public Criteria andPyquanpinIsNotNull() {
            addCriterion("PYQuanPin is not null");
            return (Criteria) this;
        }

        public Criteria andPyquanpinEqualTo(String value) {
            addCriterion("PYQuanPin =", value, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinNotEqualTo(String value) {
            addCriterion("PYQuanPin <>", value, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinGreaterThan(String value) {
            addCriterion("PYQuanPin >", value, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinGreaterThanOrEqualTo(String value) {
            addCriterion("PYQuanPin >=", value, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinLessThan(String value) {
            addCriterion("PYQuanPin <", value, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinLessThanOrEqualTo(String value) {
            addCriterion("PYQuanPin <=", value, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinLike(String value) {
            addCriterion("PYQuanPin like", value, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinNotLike(String value) {
            addCriterion("PYQuanPin not like", value, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinIn(List<String> values) {
            addCriterion("PYQuanPin in", values, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinNotIn(List<String> values) {
            addCriterion("PYQuanPin not in", values, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinBetween(String value1, String value2) {
            addCriterion("PYQuanPin between", value1, value2, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andPyquanpinNotBetween(String value1, String value2) {
            addCriterion("PYQuanPin not between", value1, value2, "pyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialIsNull() {
            addCriterion("RemarkPYInitial is null");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialIsNotNull() {
            addCriterion("RemarkPYInitial is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialEqualTo(String value) {
            addCriterion("RemarkPYInitial =", value, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialNotEqualTo(String value) {
            addCriterion("RemarkPYInitial <>", value, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialGreaterThan(String value) {
            addCriterion("RemarkPYInitial >", value, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialGreaterThanOrEqualTo(String value) {
            addCriterion("RemarkPYInitial >=", value, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialLessThan(String value) {
            addCriterion("RemarkPYInitial <", value, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialLessThanOrEqualTo(String value) {
            addCriterion("RemarkPYInitial <=", value, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialLike(String value) {
            addCriterion("RemarkPYInitial like", value, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialNotLike(String value) {
            addCriterion("RemarkPYInitial not like", value, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialIn(List<String> values) {
            addCriterion("RemarkPYInitial in", values, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialNotIn(List<String> values) {
            addCriterion("RemarkPYInitial not in", values, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialBetween(String value1, String value2) {
            addCriterion("RemarkPYInitial between", value1, value2, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyinitialNotBetween(String value1, String value2) {
            addCriterion("RemarkPYInitial not between", value1, value2, "remarkpyinitial");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinIsNull() {
            addCriterion("RemarkPYQuanPin is null");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinIsNotNull() {
            addCriterion("RemarkPYQuanPin is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinEqualTo(String value) {
            addCriterion("RemarkPYQuanPin =", value, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinNotEqualTo(String value) {
            addCriterion("RemarkPYQuanPin <>", value, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinGreaterThan(String value) {
            addCriterion("RemarkPYQuanPin >", value, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinGreaterThanOrEqualTo(String value) {
            addCriterion("RemarkPYQuanPin >=", value, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinLessThan(String value) {
            addCriterion("RemarkPYQuanPin <", value, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinLessThanOrEqualTo(String value) {
            addCriterion("RemarkPYQuanPin <=", value, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinLike(String value) {
            addCriterion("RemarkPYQuanPin like", value, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinNotLike(String value) {
            addCriterion("RemarkPYQuanPin not like", value, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinIn(List<String> values) {
            addCriterion("RemarkPYQuanPin in", values, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinNotIn(List<String> values) {
            addCriterion("RemarkPYQuanPin not in", values, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinBetween(String value1, String value2) {
            addCriterion("RemarkPYQuanPin between", value1, value2, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andRemarkpyquanpinNotBetween(String value1, String value2) {
            addCriterion("RemarkPYQuanPin not between", value1, value2, "remarkpyquanpin");
            return (Criteria) this;
        }

        public Criteria andMemberstatusIsNull() {
            addCriterion("MemberStatus is null");
            return (Criteria) this;
        }

        public Criteria andMemberstatusIsNotNull() {
            addCriterion("MemberStatus is not null");
            return (Criteria) this;
        }

        public Criteria andMemberstatusEqualTo(String value) {
            addCriterion("MemberStatus =", value, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusNotEqualTo(String value) {
            addCriterion("MemberStatus <>", value, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusGreaterThan(String value) {
            addCriterion("MemberStatus >", value, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusGreaterThanOrEqualTo(String value) {
            addCriterion("MemberStatus >=", value, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusLessThan(String value) {
            addCriterion("MemberStatus <", value, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusLessThanOrEqualTo(String value) {
            addCriterion("MemberStatus <=", value, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusLike(String value) {
            addCriterion("MemberStatus like", value, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusNotLike(String value) {
            addCriterion("MemberStatus not like", value, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusIn(List<String> values) {
            addCriterion("MemberStatus in", values, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusNotIn(List<String> values) {
            addCriterion("MemberStatus not in", values, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusBetween(String value1, String value2) {
            addCriterion("MemberStatus between", value1, value2, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andMemberstatusNotBetween(String value1, String value2) {
            addCriterion("MemberStatus not between", value1, value2, "memberstatus");
            return (Criteria) this;
        }

        public Criteria andDisplaynameIsNull() {
            addCriterion("DisplayName is null");
            return (Criteria) this;
        }

        public Criteria andDisplaynameIsNotNull() {
            addCriterion("DisplayName is not null");
            return (Criteria) this;
        }

        public Criteria andDisplaynameEqualTo(String value) {
            addCriterion("DisplayName =", value, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameNotEqualTo(String value) {
            addCriterion("DisplayName <>", value, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameGreaterThan(String value) {
            addCriterion("DisplayName >", value, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameGreaterThanOrEqualTo(String value) {
            addCriterion("DisplayName >=", value, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameLessThan(String value) {
            addCriterion("DisplayName <", value, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameLessThanOrEqualTo(String value) {
            addCriterion("DisplayName <=", value, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameLike(String value) {
            addCriterion("DisplayName like", value, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameNotLike(String value) {
            addCriterion("DisplayName not like", value, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameIn(List<String> values) {
            addCriterion("DisplayName in", values, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameNotIn(List<String> values) {
            addCriterion("DisplayName not in", values, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameBetween(String value1, String value2) {
            addCriterion("DisplayName between", value1, value2, "displayname");
            return (Criteria) this;
        }

        public Criteria andDisplaynameNotBetween(String value1, String value2) {
            addCriterion("DisplayName not between", value1, value2, "displayname");
            return (Criteria) this;
        }

        public Criteria andKeywordIsNull() {
            addCriterion("KeyWord is null");
            return (Criteria) this;
        }

        public Criteria andKeywordIsNotNull() {
            addCriterion("KeyWord is not null");
            return (Criteria) this;
        }

        public Criteria andKeywordEqualTo(String value) {
            addCriterion("KeyWord =", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordNotEqualTo(String value) {
            addCriterion("KeyWord <>", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordGreaterThan(String value) {
            addCriterion("KeyWord >", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordGreaterThanOrEqualTo(String value) {
            addCriterion("KeyWord >=", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordLessThan(String value) {
            addCriterion("KeyWord <", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordLessThanOrEqualTo(String value) {
            addCriterion("KeyWord <=", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordLike(String value) {
            addCriterion("KeyWord like", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordNotLike(String value) {
            addCriterion("KeyWord not like", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordIn(List<String> values) {
            addCriterion("KeyWord in", values, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordNotIn(List<String> values) {
            addCriterion("KeyWord not in", values, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordBetween(String value1, String value2) {
            addCriterion("KeyWord between", value1, value2, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordNotBetween(String value1, String value2) {
            addCriterion("KeyWord not between", value1, value2, "keyword");
            return (Criteria) this;
        }

        public Criteria andMemberIconIsNull() {
            addCriterion("member_icon is null");
            return (Criteria) this;
        }

        public Criteria andMemberIconIsNotNull() {
            addCriterion("member_icon is not null");
            return (Criteria) this;
        }

        public Criteria andMemberIconEqualTo(String value) {
            addCriterion("member_icon =", value, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconNotEqualTo(String value) {
            addCriterion("member_icon <>", value, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconGreaterThan(String value) {
            addCriterion("member_icon >", value, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconGreaterThanOrEqualTo(String value) {
            addCriterion("member_icon >=", value, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconLessThan(String value) {
            addCriterion("member_icon <", value, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconLessThanOrEqualTo(String value) {
            addCriterion("member_icon <=", value, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconLike(String value) {
            addCriterion("member_icon like", value, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconNotLike(String value) {
            addCriterion("member_icon not like", value, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconIn(List<String> values) {
            addCriterion("member_icon in", values, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconNotIn(List<String> values) {
            addCriterion("member_icon not in", values, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconBetween(String value1, String value2) {
            addCriterion("member_icon between", value1, value2, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andMemberIconNotBetween(String value1, String value2) {
            addCriterion("member_icon not between", value1, value2, "memberIcon");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeIsNull() {
            addCriterion("enter_group_time is null");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeIsNotNull() {
            addCriterion("enter_group_time is not null");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeEqualTo(String value) {
            addCriterion("enter_group_time =", value, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeNotEqualTo(String value) {
            addCriterion("enter_group_time <>", value, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeGreaterThan(String value) {
            addCriterion("enter_group_time >", value, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeGreaterThanOrEqualTo(String value) {
            addCriterion("enter_group_time >=", value, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeLessThan(String value) {
            addCriterion("enter_group_time <", value, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeLessThanOrEqualTo(String value) {
            addCriterion("enter_group_time <=", value, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeLike(String value) {
            addCriterion("enter_group_time like", value, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeNotLike(String value) {
            addCriterion("enter_group_time not like", value, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeIn(List<String> values) {
            addCriterion("enter_group_time in", values, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeNotIn(List<String> values) {
            addCriterion("enter_group_time not in", values, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeBetween(String value1, String value2) {
            addCriterion("enter_group_time between", value1, value2, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andEnterGroupTimeNotBetween(String value1, String value2) {
            addCriterion("enter_group_time not between", value1, value2, "enterGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeIsNull() {
            addCriterion("leave_group_time is null");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeIsNotNull() {
            addCriterion("leave_group_time is not null");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeEqualTo(String value) {
            addCriterion("leave_group_time =", value, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeNotEqualTo(String value) {
            addCriterion("leave_group_time <>", value, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeGreaterThan(String value) {
            addCriterion("leave_group_time >", value, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeGreaterThanOrEqualTo(String value) {
            addCriterion("leave_group_time >=", value, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeLessThan(String value) {
            addCriterion("leave_group_time <", value, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeLessThanOrEqualTo(String value) {
            addCriterion("leave_group_time <=", value, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeLike(String value) {
            addCriterion("leave_group_time like", value, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeNotLike(String value) {
            addCriterion("leave_group_time not like", value, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeIn(List<String> values) {
            addCriterion("leave_group_time in", values, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeNotIn(List<String> values) {
            addCriterion("leave_group_time not in", values, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeBetween(String value1, String value2) {
            addCriterion("leave_group_time between", value1, value2, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andLeaveGroupTimeNotBetween(String value1, String value2) {
            addCriterion("leave_group_time not between", value1, value2, "leaveGroupTime");
            return (Criteria) this;
        }

        public Criteria andAliasIsNull() {
            addCriterion("Alias is null");
            return (Criteria) this;
        }

        public Criteria andAliasIsNotNull() {
            addCriterion("Alias is not null");
            return (Criteria) this;
        }

        public Criteria andAliasEqualTo(String value) {
            addCriterion("Alias =", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotEqualTo(String value) {
            addCriterion("Alias <>", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThan(String value) {
            addCriterion("Alias >", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThanOrEqualTo(String value) {
            addCriterion("Alias >=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThan(String value) {
            addCriterion("Alias <", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThanOrEqualTo(String value) {
            addCriterion("Alias <=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLike(String value) {
            addCriterion("Alias like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotLike(String value) {
            addCriterion("Alias not like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasIn(List<String> values) {
            addCriterion("Alias in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotIn(List<String> values) {
            addCriterion("Alias not in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasBetween(String value1, String value2) {
            addCriterion("Alias between", value1, value2, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotBetween(String value1, String value2) {
            addCriterion("Alias not between", value1, value2, "alias");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}