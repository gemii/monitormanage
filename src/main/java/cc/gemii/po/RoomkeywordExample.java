package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class RoomkeywordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RoomkeywordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andRoomidIsNull() {
            addCriterion("RoomID is null");
            return (Criteria) this;
        }

        public Criteria andRoomidIsNotNull() {
            addCriterion("RoomID is not null");
            return (Criteria) this;
        }

        public Criteria andRoomidEqualTo(String value) {
            addCriterion("RoomID =", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotEqualTo(String value) {
            addCriterion("RoomID <>", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidGreaterThan(String value) {
            addCriterion("RoomID >", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidGreaterThanOrEqualTo(String value) {
            addCriterion("RoomID >=", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLessThan(String value) {
            addCriterion("RoomID <", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLessThanOrEqualTo(String value) {
            addCriterion("RoomID <=", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLike(String value) {
            addCriterion("RoomID like", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotLike(String value) {
            addCriterion("RoomID not like", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidIn(List<String> values) {
            addCriterion("RoomID in", values, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotIn(List<String> values) {
            addCriterion("RoomID not in", values, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidBetween(String value1, String value2) {
            addCriterion("RoomID between", value1, value2, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotBetween(String value1, String value2) {
            addCriterion("RoomID not between", value1, value2, "roomid");
            return (Criteria) this;
        }

        public Criteria andKeywordIsNull() {
            addCriterion("Keyword is null");
            return (Criteria) this;
        }

        public Criteria andKeywordIsNotNull() {
            addCriterion("Keyword is not null");
            return (Criteria) this;
        }

        public Criteria andKeywordEqualTo(String value) {
            addCriterion("Keyword =", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordNotEqualTo(String value) {
            addCriterion("Keyword <>", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordGreaterThan(String value) {
            addCriterion("Keyword >", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordGreaterThanOrEqualTo(String value) {
            addCriterion("Keyword >=", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordLessThan(String value) {
            addCriterion("Keyword <", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordLessThanOrEqualTo(String value) {
            addCriterion("Keyword <=", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordLike(String value) {
            addCriterion("Keyword like", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordNotLike(String value) {
            addCriterion("Keyword not like", value, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordIn(List<String> values) {
            addCriterion("Keyword in", values, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordNotIn(List<String> values) {
            addCriterion("Keyword not in", values, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordBetween(String value1, String value2) {
            addCriterion("Keyword between", value1, value2, "keyword");
            return (Criteria) this;
        }

        public Criteria andKeywordNotBetween(String value1, String value2) {
            addCriterion("Keyword not between", value1, value2, "keyword");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("Status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("Status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCtimeIsNull() {
            addCriterion("Ctime is null");
            return (Criteria) this;
        }

        public Criteria andCtimeIsNotNull() {
            addCriterion("Ctime is not null");
            return (Criteria) this;
        }

        public Criteria andCtimeEqualTo(String value) {
            addCriterion("Ctime =", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeNotEqualTo(String value) {
            addCriterion("Ctime <>", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeGreaterThan(String value) {
            addCriterion("Ctime >", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeGreaterThanOrEqualTo(String value) {
            addCriterion("Ctime >=", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeLessThan(String value) {
            addCriterion("Ctime <", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeLessThanOrEqualTo(String value) {
            addCriterion("Ctime <=", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeLike(String value) {
            addCriterion("Ctime like", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeNotLike(String value) {
            addCriterion("Ctime not like", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeIn(List<String> values) {
            addCriterion("Ctime in", values, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeNotIn(List<String> values) {
            addCriterion("Ctime not in", values, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeBetween(String value1, String value2) {
            addCriterion("Ctime between", value1, value2, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeNotBetween(String value1, String value2) {
            addCriterion("Ctime not between", value1, value2, "ctime");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("Type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("Type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("Type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("Type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("Type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("Type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("Type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("Type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("Type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("Type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("Type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("Type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andMatchingIsNull() {
            addCriterion("Matching is null");
            return (Criteria) this;
        }

        public Criteria andMatchingIsNotNull() {
            addCriterion("Matching is not null");
            return (Criteria) this;
        }

        public Criteria andMatchingEqualTo(String value) {
            addCriterion("Matching =", value, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingNotEqualTo(String value) {
            addCriterion("Matching <>", value, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingGreaterThan(String value) {
            addCriterion("Matching >", value, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingGreaterThanOrEqualTo(String value) {
            addCriterion("Matching >=", value, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingLessThan(String value) {
            addCriterion("Matching <", value, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingLessThanOrEqualTo(String value) {
            addCriterion("Matching <=", value, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingLike(String value) {
            addCriterion("Matching like", value, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingNotLike(String value) {
            addCriterion("Matching not like", value, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingIn(List<String> values) {
            addCriterion("Matching in", values, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingNotIn(List<String> values) {
            addCriterion("Matching not in", values, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingBetween(String value1, String value2) {
            addCriterion("Matching between", value1, value2, "matching");
            return (Criteria) this;
        }

        public Criteria andMatchingNotBetween(String value1, String value2) {
            addCriterion("Matching not between", value1, value2, "matching");
            return (Criteria) this;
        }

        public Criteria andAlerttypeIsNull() {
            addCriterion("AlertType is null");
            return (Criteria) this;
        }

        public Criteria andAlerttypeIsNotNull() {
            addCriterion("AlertType is not null");
            return (Criteria) this;
        }

        public Criteria andAlerttypeEqualTo(String value) {
            addCriterion("AlertType =", value, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeNotEqualTo(String value) {
            addCriterion("AlertType <>", value, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeGreaterThan(String value) {
            addCriterion("AlertType >", value, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeGreaterThanOrEqualTo(String value) {
            addCriterion("AlertType >=", value, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeLessThan(String value) {
            addCriterion("AlertType <", value, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeLessThanOrEqualTo(String value) {
            addCriterion("AlertType <=", value, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeLike(String value) {
            addCriterion("AlertType like", value, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeNotLike(String value) {
            addCriterion("AlertType not like", value, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeIn(List<String> values) {
            addCriterion("AlertType in", values, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeNotIn(List<String> values) {
            addCriterion("AlertType not in", values, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeBetween(String value1, String value2) {
            addCriterion("AlertType between", value1, value2, "alerttype");
            return (Criteria) this;
        }

        public Criteria andAlerttypeNotBetween(String value1, String value2) {
            addCriterion("AlertType not between", value1, value2, "alerttype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeIsNull() {
            addCriterion("KeywordType is null");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeIsNotNull() {
            addCriterion("KeywordType is not null");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeEqualTo(String value) {
            addCriterion("KeywordType =", value, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeNotEqualTo(String value) {
            addCriterion("KeywordType <>", value, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeGreaterThan(String value) {
            addCriterion("KeywordType >", value, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeGreaterThanOrEqualTo(String value) {
            addCriterion("KeywordType >=", value, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeLessThan(String value) {
            addCriterion("KeywordType <", value, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeLessThanOrEqualTo(String value) {
            addCriterion("KeywordType <=", value, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeLike(String value) {
            addCriterion("KeywordType like", value, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeNotLike(String value) {
            addCriterion("KeywordType not like", value, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeIn(List<String> values) {
            addCriterion("KeywordType in", values, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeNotIn(List<String> values) {
            addCriterion("KeywordType not in", values, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeBetween(String value1, String value2) {
            addCriterion("KeywordType between", value1, value2, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andKeywordtypeNotBetween(String value1, String value2) {
            addCriterion("KeywordType not between", value1, value2, "keywordtype");
            return (Criteria) this;
        }

        public Criteria andBelongIsNull() {
            addCriterion("Belong is null");
            return (Criteria) this;
        }

        public Criteria andBelongIsNotNull() {
            addCriterion("Belong is not null");
            return (Criteria) this;
        }

        public Criteria andBelongEqualTo(Integer value) {
            addCriterion("Belong =", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongNotEqualTo(Integer value) {
            addCriterion("Belong <>", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongGreaterThan(Integer value) {
            addCriterion("Belong >", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongGreaterThanOrEqualTo(Integer value) {
            addCriterion("Belong >=", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongLessThan(Integer value) {
            addCriterion("Belong <", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongLessThanOrEqualTo(Integer value) {
            addCriterion("Belong <=", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongIn(List<Integer> values) {
            addCriterion("Belong in", values, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongNotIn(List<Integer> values) {
            addCriterion("Belong not in", values, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongBetween(Integer value1, Integer value2) {
            addCriterion("Belong between", value1, value2, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongNotBetween(Integer value1, Integer value2) {
            addCriterion("Belong not between", value1, value2, "belong");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}