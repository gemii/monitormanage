package cc.gemii.po;

public class Robotrole {
    private String bottag;

    private String role;

    public String getBottag() {
        return bottag;
    }

    public void setBottag(String bottag) {
        this.bottag = bottag == null ? null : bottag.trim();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }
}