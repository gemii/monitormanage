package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.RoomBoundary;


public interface MonitorCustomMapper {

	List<RoomBoundary> getRoomsBySendRobot(Map<String, Object> param);

}
