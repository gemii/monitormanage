package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Membertag;



public interface MemberTagMapper {

	void deleteTag(Integer id);

	List<Membertag> getTagList(Map<String, Object> param);

	int getTagListCount(Map<String, Object> param);

}
