package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Knowledgequestion;
import cc.gemii.pojo.Knowledge;


public interface KnowledgeMapper {

	List<Knowledgequestion> getKnowledgeList(Map<String, Object> param);

	int getKnowledgeListCount(Map<String, Object> param);

	Knowledge getKnowledge(Integer id);

	void updateKnowledge(Knowledge knowledge);

	void deleteKnowledge(Integer id);

	List<Knowledge> getAllKnowledge(Integer belong);

}
