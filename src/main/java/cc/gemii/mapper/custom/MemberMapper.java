package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.MemberBoundary;

public interface MemberMapper {

	List<MemberBoundary> selectMember(Map<String, String> param);

}
