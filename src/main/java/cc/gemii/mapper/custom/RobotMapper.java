package cc.gemii.mapper.custom;

import java.util.List;

import cc.gemii.pojo.SendRobotBoundary;


public interface RobotMapper {

	Integer getMaxTag();
	
	List<SendRobotBoundary> getSendRobot(Integer id);
}
