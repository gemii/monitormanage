package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Roomkeyword;


public interface KeywordMapper {

	List<Roomkeyword> getGlobalKeyword(Map<String, Object> param);

	void enableKeyword(Integer id);

	void insertKeywordFromExcel(Map<String, Object> param);

	int getGlobalKeywordCount(Map<String, Object> param);

	List<String> getAllGlobalKeyword(Integer belong);
}
