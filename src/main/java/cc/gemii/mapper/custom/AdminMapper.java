package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.GetMonitorsBoundary;
import cc.gemii.pojo.User;

public interface AdminMapper {

	List<GetMonitorsBoundary> getMonitors();

	User login(Map<String, Object> param);

}
