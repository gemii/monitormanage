package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.RoomTag;


public interface RoomTagMapper {

	List<String> validateRoomIDs(Map<String, Object> param);

	List<RoomTag> getGlobalRoomTags(Map<String, Object> param);

	int getGlobalRoomTagsCount(Map<String, Object> param);

	void deleteRoomTag(Map<String, Object> param);

	void insertRoomTag(Map<String, Object> param);

	int isExists(String newTag);

	List<String> getRoomTag(Map<String, Object> param);

}
