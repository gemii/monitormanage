package cc.gemii.mapper;

import cc.gemii.po.Wechatroommemberinfo;
import cc.gemii.po.WechatroommemberinfoExample;
import cc.gemii.po.WechatroommemberinfoKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WechatroommemberinfoMapper {
    int countByExample(WechatroommemberinfoExample example);

    int deleteByExample(WechatroommemberinfoExample example);

    int deleteByPrimaryKey(WechatroommemberinfoKey key);

    int insert(Wechatroommemberinfo record);

    int insertSelective(Wechatroommemberinfo record);

    List<Wechatroommemberinfo> selectByExample(WechatroommemberinfoExample example);

    Wechatroommemberinfo selectByPrimaryKey(WechatroommemberinfoKey key);

    int updateByExampleSelective(@Param("record") Wechatroommemberinfo record, @Param("example") WechatroommemberinfoExample example);

    int updateByExample(@Param("record") Wechatroommemberinfo record, @Param("example") WechatroommemberinfoExample example);

    int updateByPrimaryKeySelective(Wechatroommemberinfo record);

    int updateByPrimaryKey(Wechatroommemberinfo record);
}