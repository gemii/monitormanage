package cc.gemii.mapper;

import cc.gemii.po.Membertagbind;
import cc.gemii.po.MembertagbindExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MembertagbindMapper {
    int countByExample(MembertagbindExample example);

    int deleteByExample(MembertagbindExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Membertagbind record);

    int insertSelective(Membertagbind record);

    List<Membertagbind> selectByExample(MembertagbindExample example);

    Membertagbind selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Membertagbind record, @Param("example") MembertagbindExample example);

    int updateByExample(@Param("record") Membertagbind record, @Param("example") MembertagbindExample example);

    int updateByPrimaryKeySelective(Membertagbind record);

    int updateByPrimaryKey(Membertagbind record);
}