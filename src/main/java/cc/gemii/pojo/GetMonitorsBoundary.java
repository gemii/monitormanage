package cc.gemii.pojo;

import cc.gemii.po.Monitor;

public class GetMonitorsBoundary extends Monitor{

	private Integer count;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
}
