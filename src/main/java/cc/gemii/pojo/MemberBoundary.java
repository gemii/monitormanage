package cc.gemii.pojo;

import cc.gemii.po.Wechatroommemberinfo;

public class MemberBoundary extends Wechatroommemberinfo{

	private String roomName;

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
}
