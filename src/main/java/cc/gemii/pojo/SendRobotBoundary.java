package cc.gemii.pojo;

public class SendRobotBoundary {

	private Integer id;
	
	private String RobotName;
	
	private String RobotTag;
	
	private String MonitorName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRobotName() {
		return RobotName;
	}

	public void setRobotName(String robotName) {
		RobotName = robotName;
	}

	public String getRobotTag() {
		return RobotTag;
	}

	public void setRobotTag(String robotTag) {
		RobotTag = robotTag;
	}

	public String getMonitorName() {
		return MonitorName;
	}

	public void setMonitorName(String monitorName) {
		MonitorName = monitorName;
	}
	
}
